<?php

/**
 * @file
 *   Admin settings for og_statistics module.
 */
function og_statistics_settings() {
  $form['rebuild'] = array(
    '#type' => 'submit',
    '#value' => t('Rebuild Organic Groups Statistics')
  );

  return $form;
}

function og_statistics_settings_recalc($group_type, &$context) {
  if (empty($context['sandbox'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['offset'][$group_type] = 0;
    // Count the number of groups.
    $query = new EntityFieldQuery();
    $query
      ->fieldCondition('group_group', 'value', 1)
      ->count();
    $context['sandbox']['max'] = $query->execute();
  }

  // Query a number of groups.
  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', $group_type)
    ->fieldCondition('group_group', 'value', 1)
    ->entityOrderBy('entity_id', 'ASC')
    ->range($context['sandbox']['offset'][$group_type], 10);
  $entities = $query->execute();

  if (!isset($entities[$group_type])) {
    $context['finished'] = TRUE;
    return;
  }

  // Write a new record with calculated numbers for each one
  foreach ($entities[$group_type] as $gid => $group) {
    // Content count.
    $query = db_select('og_membership', 'm')
      ->condition('m.group_type', $group_type)
      ->condition('m.gid', $gid)
      ->condition('m.state', 1);
    if (!$content_count = $query->countQuery()->execute()->fetchField()) {
      $content_count = 0;
    }

    // Membership count.
    $query = db_select('og_membership', 'm')
      ->condition('m.entity_type', 'user')
      ->condition('m.field_name', 'og_user_' . $group_type)
      ->condition('m.group_type', $group_type)
      ->condition('m.gid', $gid)
      ->condition('m.state', 1);
    if (!$members_count = $query->countQuery()->execute()->fetchField()) {
      $members_count = 0;
    }

    // Comment count.
    $comments_count = 0;
    if (module_exists('comment')) {
      $query = db_select('og_membership', 'm');
      $query->leftJoin('node_comment_statistics', 'ncs', 'ncs.nid = m.etid');
      $query
        ->condition('m.entity_type', 'node')
        ->condition('m.group_type', $group_type)
        ->condition('m.gid', $gid)
        ->condition('m.state', 1);
      $query->addExpression('SUM(ncs.comment_count)', 'comment_count');
      if (!$comments_count = $query->execute()->fetchField()) {
        $comments_count = 0;
      }
    }

    // Last node.
    $last_node_timestamp = NULL;
    $last_node_nid = NULL;
    $last_node_uid = NULL;
    $query = db_select('og_membership', 'm');
    $query->innerJoin('node', 'n', 'n.nid = m.etid');
    $query
      ->condition('m.entity_type', 'node')
      ->condition('m.group_type', $group_type)
      ->condition('m.gid', $gid)
      ->condition('m.state', 1);
    $query
      ->fields('n')
      ->orderBy('n.created', 'DESC')
      ->range(0, 1);
    if ($last_node = $query->execute()->fetchObject()) {
      $last_node_timestamp = $last_node->created;
      $last_node_nid = $last_node->nid;
      $last_node_uid = $last_node->uid;
    }

    // Last member.
    $last_member_timestamp = NULL;
    $last_member_uid = NULL;
    $query = db_select('og_membership', 'm');
    $query->innerJoin('users', 'u', 'u.uid = m.etid');
    $query
      ->condition('m.entity_type', 'user')
      ->condition('m.field_name', 'og_user_' . $group_type)
      ->condition('m.group_type', $group_type)
      ->condition('m.gid', $gid)
      ->condition('m.state', 1);
    $query
      ->fields('u')
      ->orderBy('m.created', 'DESC')
      ->range(0, 1);
    if ($last_member = $query->execute()->fetchObject()) {
      $last_member_timestamp = $last_member->created;
      $last_member_uid = $last_member->uid;
    }

    // Last comment.
    $last_comment_timestamp = NULL;
    $last_comment_nid = NULL;
    $last_comment_uid = NULL;
    $last_comment_cid = NULL;
    $query = db_select('og_membership', 'm');
    $query->innerJoin('comment', 'c', 'c.nid = m.etid');
    $query
      ->condition('m.entity_type', 'node')
      ->condition('m.group_type', $group_type)
      ->condition('m.gid', $gid)
      ->condition('m.state', 1);
    $query
      ->fields('c')
      ->orderBy('c.created', 'DESC')
      ->range(0, 1);
    if ($last_comment = $query->execute()->fetchObject()) {
      $last_comment_timestamp = $last_comment->created;
      $last_comment_nid = $last_comment->nid;
      $last_comment_uid = $last_comment->uid;
      $last_comment_cid = $last_comment->cid;
    }

    $last_activity_timestamp = max($last_node_timestamp, $last_member_timestamp, $last_comment_timestamp);

    db_insert('og_statistics')
      ->fields(array(
        'group_type' => $group_type,
        'gid' => $gid,
        'members_count' => $members_count,
        'content_count' => $content_count,
        'comments_count' => $comments_count,
        'last_activity' => $last_activity_timestamp,
        'last_node_timestamp' => $last_node_timestamp,
        'last_comment_timestamp' => $last_comment_timestamp,
        'last_member_timestamp' => $last_member_timestamp,
        'last_comment_uid' => $last_comment_uid,
        'last_comment_nid' => $last_comment_nid,
        'last_comment_cid' => $last_comment_cid,
        'last_node_nid' => $last_node_nid,
        'last_node_uid' => $last_node_uid,
        'last_member_uid' => $last_member_uid,
      ))
      ->execute();
    $context['results'][] = $group_type . ':' . $gid;
    $context['sandbox']['progress']++;
    $context['sandbox']['offset'][$group_type]++;
  }

  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

function og_statistics_settings_submit() {
  db_truncate('og_statistics')->execute();

  $batch = array(
    'title' => t('Generating statistics'),
    'operations' => array(),
    'progress_message' => t('Calculated statistics for @current out of @total group types'),
    'finished' => 'og_statistics_settings_finished',
    'file' => drupal_get_path('module', 'og_statistics') . '/og_statistics.admin.inc',
  );

  // Create a separate operation for each group type.
  foreach (og_get_all_group_entity() as $group_type => $group_type_label) {
    $batch['operations'][] = array('og_statistics_settings_recalc', array($group_type));
  }

  batch_set($batch);
}

function og_statistics_settings_finished($success, $results, $operations) {
  if ($success) {
    $message = format_plural(count($results), 'One group processed.', '@count groups processed.');
  } else {
    $message = t('Finished with an error.');
  }
  drupal_set_message($message);
}
